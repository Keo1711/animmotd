# This project is officially inactive #

### What is AnimMOTD ###
AnimMOTD is a plugin that allows your server to have a animated MOTD and server icon.

### How do I set this up? ###
Read this wiki page on setting it up. It's really easy! 6 steps actually...

### WARNING / DISCLAIMER ###
IN NO EVENT, UNLESS REQUIRED BY APPLICABLE 
LAW OR AGREED TO IN WRITING, SHALL ANY PERSON BE LIABLE FOR ANY LOSS, EXPENSE OR 
DAMAGE, OF ANY TYPE OR NATURE ARISING OUT OF 
THE USE OF, OR INABILITY TO USE THIS SOFTWARE 
OR PROGRAM, INCLUDING, BUT NOT LIMITED TO, 
CLAIMS, SUITS OR CAUSES OF ACTION INVOLVING 
ALLEGED INFRINGEMENT OF COPYRIGHTS, PATENTS, 
TRADEMARKS, TRADE SECRETS, OR UNFAIR 
COMPETITION.