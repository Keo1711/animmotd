/* 
 * AnimatedMOTD BungeePlugin
 * Copyright (C) 2014 Sander Gielisse
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.lite20.animatedMOTD.ping;

import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;

import com.lite20.animatedMOTD.MOTD;
import com.lite20.animatedMOTD.Main;
import com.lite20.animatedMOTD.Methods;
import com.lite20.animatedMOTD.PlayerCount;
import com.lite20.animatedMOTD.PlayerList;
import com.lite20.animatedMOTD.ServerIcon;

public class DefaultPingManager implements StatusListener {
	MOTD motd = new MOTD();
	ServerIcon icon = new ServerIcon();
	boolean repeat = false;
	String motd1 = "";
	String motd2 = "";
	String pcount = "";
	BufferedImage frame = null;
	PlayerList playerlist = new PlayerList();
	PlayerCount playercount = new PlayerCount();
	List<String> plist = new ArrayList<String>();
	
	@Override
	public ServerData update() {
		if(!repeat) {
			try {
				if(Main.configuration.getBoolean("AnimatePlayerList")) {
					plist = playerlist.getNextList();
				
				}
				
				if(Main.configuration.getBoolean("AnimatePlayerCount")) {
					pcount = playercount.getNextCount();
				
				}
				
				motd1 = motd.getNextLine1();
				motd1 = Methods.handle_variables(motd1);
				motd2 = motd.getNextLine2();
				motd2 = Methods.handle_variables(motd2);
				frame = icon.getNextFrame();
				
			} 
			
			catch (IllegalAccessException e) {
				e.printStackTrace();
				
			} 
			
			catch (IllegalArgumentException e) {
				e.printStackTrace();
				
			} 
			
			catch (InvocationTargetException e) {
				e.printStackTrace();
				
			} 
			
			catch (NoSuchMethodException e) {
				e.printStackTrace();
				
			} 
			
			catch (SecurityException e) {
				e.printStackTrace();
				
			} 
			
			catch (InstantiationException e) {
				e.printStackTrace();
				
			}
		}

		repeat = !repeat;
		if(Main.configuration.getBoolean("AnimatePlayerList") && Main.configuration.getBoolean("AnimatePlayerCount")) {
			return new ServerData(motd1, motd2, frame, Main.configuration.getInt("update_delay"), pcount, plist);
			
		}
		
		else if(Main.configuration.getBoolean("AnimatePlayerList")) {
			return new ServerData(motd1, motd2, frame, Main.configuration.getInt("update_delay"), "" + ChatColor.GRAY + ProxyServer.getInstance().getOnlineCount() + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + "%MAX%", plist);
			
		}

		else {
			return new ServerData(motd1, motd2, frame, Main.configuration.getInt("update_delay"), "" + ChatColor.GRAY + ProxyServer.getInstance().getOnlineCount() + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + "%MAX%");

		}
	}
}
