package com.lite20.animatedMOTD;

import net.md_5.bungee.api.ChatColor;

public class PlayerCount {
	int position = 0;
	
	public PlayerCount() {
		try {
			if(Main.configuration.getBoolean("live_edit")) {
				Main.pcount = Methods.readLines(Main.playercount);

			}
			
			if(Main.pcount.isEmpty()) {
				Main.pcount.add(ChatColor.GRAY + "%COUNT%" + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + "%MAX%");
				
			}
		}
		
		catch(Exception e) {
			e.printStackTrace();
			
		}
	}

	public String getNextCount() {
		if(position > (Main.pcount.size() - 1)) {
			position = 0;
			
		}

		int retpos = position;
		position++;
		return Main.pcount.get(retpos);

	}
}
