package com.lite20.animatedMOTD;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlayerList {
	int position = 0;

	public PlayerList() {
		try {
			if(Main.configuration.getBoolean("live_edit")) {
				File[] listOfFiles = Main.playerlistframes.listFiles(); 
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						Main.lists.add(Methods.readLines(listOfFiles[i]));
						
					}
				}	
				
				if(Main.lists.isEmpty()) {
					List<String> list = new ArrayList<String>();
					list.add(" ");
					Main.lists.add(list);
					
				}
			}
		}
		
		catch(Exception e) {
			e.printStackTrace();
			
		}
	}

	public List<String> getNextList() {
		if(position > (Main.lists.size() - 1)) {
			position = 0;
			
		}

		int retpos = position;
		position++;
		return Main.lists.get(retpos);

	}
}
