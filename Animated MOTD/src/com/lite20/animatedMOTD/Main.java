/* 
 * AnimatedMOTD BungeePlugin
 * Copyright (C) 2014 Sander Gielisse
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.lite20.animatedMOTD;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.netty.PipelineUtils;

public class Main extends Plugin {
	public static Configuration configuration;
	public static List<Class<?>> variables;
	public static List<String> motd1 = new ArrayList<String>();
	public static List<String> motd2 = new ArrayList<String>();
	public static List<String> pcount = new ArrayList<String>();
	public static List<List<String>> lists = new ArrayList<List<String>>();
	public static List<BufferedImage> frames = new ArrayList<BufferedImage>();
	public static File dir;
	public static File var_dir;
	public static File icon_dir;
	public static File motdline1;
	public static File motdline2;
	public static File playerlistframes;
	public static File playercount;
	
	@Override
	public void onEnable() {
		try {
			this.setStaticFinalValue(PipelineUtils.class.getDeclaredField("SERVER_CHILD"), new ConnectionReplacement(this));
			try {
				URL url = new URL("http://lf.sdf.org/animmotd/nv.html");
				InputStream is = (InputStream) url.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = null;
				StringBuffer sb = new StringBuffer();
				while((line = br.readLine()) != null){
					sb.append(line);
				   
				}
				 
				if(!sb.toString().contains("v1.9")) {
					System.out.println("[AnimMOTD] UPDATE DETECTED! ");
					System.out.println("[AnimMOTD] This version of AnimMOTD is outdated. Please update to the newest version. ");

				}
			}
			
			catch (Exception e) {
				System.out.println("[AnimMOTD] Could not check for new version. This version might be outdated... ");
				
			} 
			
			dir = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//");
			if(!dir.exists()) {
				dir.mkdir();
				
			}
			
			var_dir = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//variables//");
			if(!var_dir.exists()) {
				var_dir.mkdir();
				
			}
			
			icon_dir = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//server-icon-frames//");
			if(!icon_dir.exists()) {
				icon_dir.mkdir();
				
			}
			
			motdline1 = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//motd_line1.txt");
			if(!motdline1.exists()) {
				motdline1.createNewFile();
				
			}
			
			motdline2 = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//motd_line2.txt");
			if(!motdline2.exists()) {
				motdline2.createNewFile();
				
			}
			
			
			playerlistframes = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//player-list-frames//");
			if(!playerlistframes.exists()) {
				playerlistframes.mkdir();
				
			}
			
			File file = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//player-list-frames//1.txt");
			if(!file.exists()) {
				file.createNewFile();
				
			}
			
			playercount = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//player_count.txt");
			if(!playercount.exists()) {
				playercount.createNewFile();
				
			}
			
            File conf = new File(getDataFolder(), "config.yml");
            if (!conf.exists()) {
                Files.copy(getResourceAsStream("config.yml"), conf.toPath());
                
            }
            
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
            variables = Methods.getVariables();
            
			motd1 = Methods.readLines(motdline1);
			motd2 = Methods.readLines(motdline2);
			pcount = Methods.readLines(playercount);
			if(pcount.isEmpty()) {
				pcount.add(ChatColor.GRAY + "%COUNT%" + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + "%MAX%");
				
			}
			
			File[] listOfFiles = playerlistframes.listFiles(); 
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					lists.add(Methods.readLines(listOfFiles[i]));
					
				}
			}	
			
			if(lists.isEmpty()) {
				List<String> list = new ArrayList<String>();
				list.add(" ");
				lists.add(list);
				
			}
			
			frames = Methods.readImages(icon_dir);
			
		}
		
		catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	private void setStaticFinalValue(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
		
	}
}
