/* 
 * AnimatedMOTD BungeePlugin
 * Copyright (C) 2014 Ephraim Bilson
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.lite20.animatedMOTD;

import java.awt.image.BufferedImage;

public class ServerIcon {
	int position = 0;
	
	public ServerIcon() {
		if(Main.configuration.getBoolean("live_edit")) {
			Main.frames = Methods.readImages(Main.icon_dir);
		
		}
	}
	
	
	
	public BufferedImage getNextFrame() {
		if(Main.frames.size() > 0) {
			if(position > (Main.frames.size() - 1)) {
				position = 0;
				
			}
			
			int retpos = position;
			position++;
			return Main.frames.get(retpos);
			
		}
		
		else {
			return null;
			
		}
	}
}
