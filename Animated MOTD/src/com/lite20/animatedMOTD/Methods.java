/* 
 * AnimatedMOTD BungeePlugin
 * Copyright (C) 2014 Ephraim Bilson
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.lite20.animatedMOTD;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import net.md_5.bungee.api.ProxyServer;

public class Methods {
	public static String handle_variables(String result) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(Main.configuration.getBoolean("use_AND_as_color_symbol")) {
			result = result.replaceAll("&", "�");
			
		}
		
		result = result.replaceAll("%COUNT%", "" + ProxyServer.getInstance().getOnlineCount());
		for(int i = 0; i < Main.variables.size(); i++) {
			result = result.replaceAll((String)Main.variables.get(i).getMethod("getTag").invoke(Main.variables.get(i).newInstance()),(String)Main.variables.get(i).getMethod("getValue").invoke(Main.variables.get(i).newInstance())); 
			
		}
		
		return result;
		
	}
	
	@SuppressWarnings("deprecation")
	public static List<Class<?>> getVariables() {
		List<Class<?>> vars = new ArrayList<Class<?>>();
		File plugFolder = new File(Methods.getDataFolder().getPath() + "//AnimMOTD//variables//");
		for(int i = 0; i < plugFolder.listFiles().length; i++) {
			try {
				URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
						plugFolder.listFiles()[i].toURL()
						
				});
				
				Class<?> clazz = classLoader.loadClass("com.animmotd.Var");
				vars.add(clazz);
				System.out.println("[AnimMOTD] Loaded variable '" + (String)clazz.getMethod("getTag").invoke(clazz.newInstance()) + "'. ");

			}
			
			catch(Exception e) {
				e.printStackTrace();
				
			}
		}	
		
		return vars;
		
	}
	
	public static File getDataFolder() {
		try {
			return new File(new File(MOTD.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent());
			
		}
		
		catch (URISyntaxException e) {
			e.printStackTrace();
			
		}
		
		return null;
		
	}
	
	public static List<BufferedImage> readImages(File dir) {
		List<BufferedImage> images = new ArrayList<BufferedImage>();
		try {
			File[] listOfFiles = dir.listFiles(); 
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if(listOfFiles[i].getName().endsWith(".png")) {
						images.add(ImageIO.read(listOfFiles[i]));
						
					}
				}
			}	
		}
			
		catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return images;
		
	}
	
	public static List<String> readLines(File file) {
		List<String> lines = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
			   lines.add(line);
			   
			}
			
			br.close();
			
		}
			
		catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} 
		
		catch (IOException e) {
			e.printStackTrace();
			
		}
		
		return lines;
		
	}
}
