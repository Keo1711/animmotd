/* 
 * AnimatedMOTD BungeePlugin
 * Copyright (C) 2014 Ephraim Bilson
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.lite20.animatedMOTD;

public class MOTD {
	int position = 0;
	int position2 = 0;
	
	public MOTD() {
		try {
			if(Main.configuration.getBoolean("live_edit")) {
				Main.motd1 = Methods.readLines(Main.motdline1);
				Main.motd2 = Methods.readLines(Main.motdline2);
				
			}

			if(Main.motd1.isEmpty()) {
				Main.motd1.add(" ");
				
			}
			
			if(Main.motd2.isEmpty()) {
				Main.motd2.add(" ");
				
			}
		}
		
		catch(Exception e) {
			e.printStackTrace();
			
		}
	}

	public String getNextLine1() {
		if(position > (Main.motd1.size() - 1)) {
			position = 0;
			
		}

		int retpos = position;
		position++;
		return Main.motd1.get(retpos);

	}
	
	public String getNextLine2() {
		if(position2 > (Main.motd2.size() - 1)) {
			position2 = 0;
			
		}
		
		int retpos = position2;
		position2++;
		return Main.motd2.get(retpos);

	}
}
